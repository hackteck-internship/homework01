import random


def print_banner():
    print("  ___  _  _  ____  ____  ____    __  ____ ")
    print(" / __)/ )( \(  __)/ ___)/ ___)  (  )(_  _)")
    print("( (_ \) \/ ( ) _) \___ \\\\___ \   )(   )(  ")
    print(" \___/\____/(____)(____/(____/  (__) (__) \n")

def print_rules():
    print("\n       *** Rules of the Game ***       \n\n")
    print("* You are going to guess a number between 0 and 10\n")
    print("* The bot will tell wheather your guess is higher or lower than the correct number\n")
    print("* If your guess is correct 'Victory' will be displayed!\n")
    print("* You will be prompted to play again after the correct guess\n")

def play():
    print("\n*** We already picked  a number! Try to guess it ***\n\n")

    guess = random.randint(0, 10)
    while (True):
        val = input("Your guess: ")
        if(not(val.isnumeric())):
            print("Please input an integer between 0 and 10\n")
            continue
        elif (int(val) < guess):
            print("It is less than needed!\n")
        elif (int(val) > guess):
            print("It is bigger than needed!\n")
        elif (int(val) == guess):
            print("Victory!\n")
            break

def play_again():
    answer = input("\nWould you like to play again ? (y/n): ")

    while (True):
        if (answer == "y"):
            return True
        elif (answer == "n"):
            return False
        else:
            answer = input("Please write 'y' or 'n': ")
            continue

def start_game():
    print_banner()
    print_rules()

    while (True):
        play()
        if (not(play_again())):
            print("Thank you for the game :). Goodbye!")
            break

if (__name__ == '__main__'):
    start_game()
