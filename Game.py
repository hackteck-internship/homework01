import random


''' A class representing a player. '''

class Player:

    def __init__(self, name):
        self.name = name

    def get_name(self):
        return self.name

    def get_number_of_guesse(self):
        return self._number_of_guesses

    def set_name(self, name):
        self.name = name

    def make_guess(self):

        while (True):
            player_guess = input("Your guess : ")

            if not player_guess.isnumeric():
                print("Please input an integer between 0 and 10\n")
                continue
            else:
                break

        return int(player_guess)

    def play_again(self):
        answer = input("\nWould you like to play again? (y/n): ")
        result = False

        while (True):

            if (answer == "y"):
                result = True
                break
            elif (answer == "n"):
                result = False
                break
            else:
                answer = input("Please write 'y' or 'n': ")
                continue

        return result


''' A class representing the game. '''

class Game:

    _game_guess = -1

    def __init__(self, name="Undefined"):
        self._player = Player(name)

    def _print_banner(self):
        print("  ___  _  _  ____  ____  ____    __  ____ ")
        print(" / __)/ )( \(  __)/ ___)/ ___)  (  )(_  _)")
        print("( (_ \) \/ ( ) _) \___ \\\\___ \   )(   )(  ")
        print(" \___/\____/(____)(____/(____/  (__) (__) \n")

    def _print_rules(self):
        print("\n       *** Rules of the Game ***       \n\n")
        print("* You are going to guess a number between 0 and 10\n")
        print("* The bot will tell wheather your guess is higher or lower than the correct number\n")
        print("* If your guess is correct 'Victory' will be displayed!\n")
        print("* You will be prompted to play again after the correct guess\n")

    def _print_goodbye(self):
        print(f"\n       *** We are sorry to see you go {self._player.get_name()} ***       ")
        print(f"        Thank you for a wonderful game, goodbye :)")

    def _make_random_guess(self):
        self._game_guess = random.randint(0, 10)

    def _check_player_guess(self, player_guess):
        status = ""

        if (player_guess < self._game_guess):
            status = "It is less than needed!"
        elif (player_guess > self._game_guess):
            status = "It is bigger than needed!\n"
        else:
            status = "Victory!"

        return status

    def _play(self):
        print(f"\n*** We already picked a number! Try to guess it {self._player.get_name()} ***\n\n")

        self._make_random_guess()
        while (True):

            player_guess = self._player.make_guess()
            status = self._check_player_guess(player_guess)
            print(status)

            if (status == "Victory!"):
                break

    def start_game(self):
        self._print_banner()
        self._print_rules()

        while (True):
            self._play()

            if not self._player.play_again():
                self._print_goodbye()
                break


if (__name__ == '__main__'):
    name = input("What is your name?: ")
    print(f"\n    *** Welcom to the number guessing game {name}! *** \n\n")
    game = Game(name)
    game.start_game()
